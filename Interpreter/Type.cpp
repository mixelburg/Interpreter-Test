#include "type.h"

bool Type::isIsTemp() const
{
	return _isTemp;
}

void Type::setIsTemp(const bool isTemp)
{
	_isTemp = isTemp;
}

bool Type::isPrintable() const
{
	return true;
}

void Type::switchTemp()
{
	_isTemp = !_isTemp;
}

std::string Type::getType() const
{
	return _type;
}
