#ifndef TYPE_H
#define TYPE_H
#include <string>

class Type
{
public:
	virtual ~Type() = default;
	bool isIsTemp() const;
	void setIsTemp(const bool isTemp);
	virtual bool isPrintable() const;
	virtual std::string toString() const = 0;
	void switchTemp();
	std::string getType() const;
private:
	bool _isTemp = false;
protected:
	std::string _type;

};



#endif //TYPE_H
