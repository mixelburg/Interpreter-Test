#ifndef INTEGER_H
#define INTEGER_H

#include "type.h"
class Integer : public Type
{
public:
	explicit Integer(const int val);
	std::string toString() const override;
private:
	int _val;
	
};

#endif // INTEGER_H