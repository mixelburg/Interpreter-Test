#include "parser.h"
#include "IndentationException.h"
#include "NameErrorException.h"
#include "SyntaxException.h"
#include "Helper.h"
#include "Boolean.h"
#include "Integer.h"
#include "String.h"
#include "Void.h"
#include "List.h"


Type* Parser::parseString(std::string str) throw()
{
	if (str.length() == 0)
	{
		throw IndentationException();
	}
	if(str[0] == ' ')
	{
		throw IndentationException();
	}
	else if(str[0] == '\t')
	{
		throw IndentationException();
	}
	else
	{
		Helper::trim(str);

		if (isCommand(str))
		{
			if (str[0] == 't')
			{
				Type* val = parseString(getCommandValue(str));
				if (val != nullptr)
				{
					std::cout << " type: " << val->getType() << std::endl;
				}
			}
			else if (str[0] == 'd')
			{
				_variables.erase(getCommandValue(str));
			}
			else if (str[0] == 'l')
			{
				Type* val = parseString(getCommandValue(str));
				if (val != nullptr)
				{
					if (val->getType() == "String" || val->getType() == "List")
					{
						auto* temp = dynamic_cast<Sequence*>(val);
						Type* ret = new Integer(temp->getLen());
						ret->setIsTemp(true);
						return ret;
					}
				}

			}
		}
		else
		{
			Type* type = getType(str);
			if (type)
			{
				type->setIsTemp(true);
				return type;
			}
			else
			{
				if (makeAssignment(str))
				{
					type = new Void();
					type->setIsTemp(true);
					return type;
				}
				else
				{
					type = getVariableValue(str);
					if (type) return type;
				}
			}
		}
	}
	
	return nullptr;
}

bool Parser::isCommand(const std::string& str)
{
	if (str[str.length() - 1] != ')') return false;
	auto pos = str.find('(');
	std::string start = str.substr(0, pos);

	if (start == "type" || start == "del" || start == "len") return true;
}

std::string Parser::getCommandValue(const std::string& str)
{
	std::string temp = str;
	auto pos = temp.find('(');
	temp = temp.substr(1, temp.size() - 2);
	return temp.substr(pos);
}


Type* Parser::getType(std::string& str)
{
	Type* type = nullptr;

	if (Helper::isBoolean(str))
	{
		type = new Boolean(Helper::toBool(str));
	}
	if (Helper::isInteger(str))
	{
		type = new Integer(Helper::toInt(str));
	}
	if (Helper::isString(str))
	{
		type = new String(str);
	}
	if (Helper::isList(str))
	{
		std::string s = str.substr(1, str.size() - 2);
		const std::string delim = ",";
		std::vector<std::string> tokens;
		size_t prev = 0, pos = 0;
		do
		{
			pos = s.find(delim, prev);
			if (pos == std::string::npos) pos = s.length();
			std::string token = s.substr(prev, pos - prev);
			if (!token.empty()) tokens.push_back(token);
			prev = pos + delim.length();
		} while (pos < s.length() && prev < s.length());

		for (auto& token : tokens)
		{
			Helper::trim(token);
		}

		std::vector<Type*> elems;
		elems.reserve(tokens.size());
		for (auto& token : tokens)
		{
			Type* elem = getType(token);
			elem->setIsTemp(false);
			elems.push_back(elem);
		}
		
		type = new List(elems);
	}

	return type;
}

bool Parser::isLegalVarName(const std::string& str)
{
	if (Helper::isDigit(str[0])) return false;
	for (int i = 1; i < str.length(); i++)
	{
		if (!(Helper::isDigit(str[i]) ||
			Helper::isLetter(str[i]) ||
			Helper::isUnderscore(str[i])))
			return false;
	}
	return true;
}

bool Parser::makeAssignment(const std::string& str)
{
	int occurrences = 0;
	std::string::size_type pos = 0;
	const std::string target = "=";
	while ((pos = str.find(target, pos)) != std::string::npos) {
		++occurrences;
		pos += target.length();
	}
	if (occurrences != 1) return false;

	std::string name = str.substr(0, str.find(target));
	std::string val = str.substr(str.find(target) + target.length(), str.length());
	Helper::trim(name);
	Helper::trim(val);

	auto it_name = Parser::_variables.find(name);
	auto it_val = Parser::_variables.find(val);
	if(it_name != Parser::_variables.end() && it_val != Parser::_variables.end())
	{
		if (it_val->second->getType() == "List")
		{
			it_name->second = it_val->second;
		}
		else
		{
			std::string v = it_val->second->toString();
			Type* copy = getType(v);
			it_name->second = copy;

		}
		return true;
	}

	if (!isLegalVarName(name)) throw NameErrorException(name);
	Type* value = getType(val);
	if (!value) throw SyntaxException();
	auto it = Parser::_variables.find(name);
	if (it != Parser::_variables.end())
	{
		it->second = value;
	}
	else
	{
		Parser::_variables.insert(std::make_pair(name, value));
	}
	return true;
}

Type* Parser::getVariableValue(const std::string& str)
{
	Type* value = nullptr;

	auto it = Parser::_variables.find(str);
	if (it == Parser::_variables.end()) throw NameErrorException(str);
	else value = it->second;

	return value;
}

void Parser::clearMemory()
{
	for (const auto& element : _variables)
	{
		delete element.second;
	}
}


