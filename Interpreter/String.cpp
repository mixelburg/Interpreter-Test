#include "String.h"

#include <utility>

bool String::isPrintable() const
{
	return true;
}

String::String(std::string val) : _val(std::move(val))
{
	_type = "String";
}

std::string String::toString() const
{
	return _val;
}

int String::getLen() const
{
	return _val.length();
}

