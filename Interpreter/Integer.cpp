#include "Integer.h"

Integer::Integer(const int val)
{
	_val = val;
	_type = "Integer";
}

std::string Integer::toString() const
{
	return std::to_string(_val);
}

