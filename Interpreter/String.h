#ifndef STRING_H
#define STRING_H

#include "Sequence.h"
#include "string"
class String : public Sequence
{
public:
	bool isPrintable() const override;
	explicit String(std::string val);
	std::string toString() const override;
	int getLen() const override;
private:
	std::string _val;
};

#endif // STRING_H