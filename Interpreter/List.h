#ifndef LIST_H
#define LIST_H

#include <vector>
#include "Sequence.h"

class List : public Sequence
{
public:
	explicit List(const std::vector<Type*>& values);
	~List() override;
	std::string toString() const override;
	bool isPrintable() const override;
	int getLen() const override;
private:
	std::vector<Type*> _values;
};


#endif // LIST_H