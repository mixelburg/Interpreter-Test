#include "NameErrorException.h"

#include <utility>

NameErrorException::NameErrorException(std::string name) : _name(std::move(name))
{
	_msg = "NameError : name [" + _name + "] is not defined";
}

const char* NameErrorException::what() const throw()
{
	return _msg.c_str();
}
