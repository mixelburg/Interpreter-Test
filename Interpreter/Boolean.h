#ifndef BOOLEAN_H
#define BOOLEAN_H

#include "type.h"
class Boolean : public Type
{
public:
	Boolean(const bool val);
	std::string toString() const override;
private:
	bool _val;
};

#endif // BOOLEAN_H