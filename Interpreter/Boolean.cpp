#include "Boolean.h"

#include <ios>

Boolean::Boolean(const bool val)
{
	_val = val;
	_type = "Boolean";
}

std::string Boolean::toString() const
{
	return _val ? "true" : "false";
}

