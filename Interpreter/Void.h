#ifndef VOID_H
#define VOID_H

#include "type.h"
class Void : public Type
{
public:
	Void();
	bool isPrintable() const override;
	std::string toString() const override;
};

#endif // VOID_H