#ifndef SEQUENCE_H
#define SEQUENCE_H

#include "type.h"
class Sequence : public Type
{
public:
	virtual int getLen() const = 0;
};


#endif // SEQUENCE_H
