#include "type.h"
#include "InterperterException.h"
#include "parser.h"
#include <iostream>

#define WELCOME "Welcome to Magshimim Python Interperter version 1.0 by "
#define YOUR_NAME "[mixelburg]"

std::unordered_map<std::string, Type*> Parser::_variables;
int main(int argc,char **argv)
{	
	std::cout << WELCOME << YOUR_NAME << std::endl;

	std::string input_string;
	
	// get new command from user
	std::cout << ">>> ";
	std::getline(std::cin, input_string);
	
	while (input_string != "quit()")
	{
		try
		{
			// prasing command
			Type* type = Parser::parseString(input_string);
			if (type != nullptr)
			{
				if (type->isPrintable())
				{
					std::cout << type->toString() << std::endl;
				}
				if (type->isIsTemp())
				{
					delete type;
				}
			}
		}
		catch (const InterperterException& e)
		{
			std::cout << e.what() << std::endl;
		}
		catch (const std::exception& e)
		{
			std::cout << e.what() << std::endl;
		}
		catch (...)
		{
		}
		

		// get new command from user
		std::cout << ">>> ";
		std::getline(std::cin, input_string);
	}

	Parser::clearMemory();

	return 0;
}


