#include "IndentationException.h"

const char* IndentationException::what() const throw()
{
	return "Indentation Error: invalid syntax";
}