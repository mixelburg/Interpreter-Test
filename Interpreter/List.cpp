#include "List.h"

#include <iostream>

List::List(const std::vector<Type*>& values)
{
	_values = values;
	_type = "List";
}

List::~List()
{
	for (auto* element : _values)
	{
		delete element;
	}
}

std::string List::toString() const
{
	std::string s = "[ ";
	for (auto* element : _values)
	{
		s += element->toString();
		s += ", ";
	}
	return s += " ]";
}

bool List::isPrintable() const
{
	return true;
}

int List::getLen() const
{
	return _values.size();
}
