#ifndef NAME_ERROR_EXCEPTION_H
#define NAME_ERROR_EXCEPTION_H
#include <string>

#include "InterperterException.h"

class NameErrorException : public InterperterException
{
public:
	NameErrorException(std::string name);
	virtual const char* what() const throw();
private:
	const std::string _name;
	std::string _msg;
};


#endif // NAME_ERROR_EXCEPTION_H